//
//  ViewController.swift
//  ScrollViewTest
//
//  Created by Maurício Marques on 8/19/17.
//  Copyright © 2017 Maurício Marques. All rights reserved.
//

import UIKit
import SnapKit

class ViewController: UIViewController {
    
    let scrollView = UIScrollView()
    let containerView = UIView()
    let button = UIButton()
    let header = UIView()
    let footer = UIView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.view.addSubview(self.scrollView)
        self.view.addSubview(self.button)
        
        self.scrollView.addSubview(self.containerView)
        self.containerView.addSubview(self.header)
        
        self.scrollView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        self.button.snp.makeConstraints { (make) in
            make.trailing.equalToSuperview().offset(-20.0)
            make.bottom.equalToSuperview().offset(-20.0)
        }
        
        self.containerView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
            make.width.equalToSuperview()
            make.height.equalToSuperview().priority(250.0)
        }
        
        self.header.snp.makeConstraints { (make) in
            make.leading.top.trailing.equalToSuperview()
            make.width.equalToSuperview()
            make.height.equalTo(150.0)
        }
        
        self.button.setTitleColor(UIColor.blue, for: .normal)
        self.button.setTitle("Plus", for: .normal)
        self.button.addTarget(self, action: #selector(action), for: .touchUpInside)
        
        self.header.backgroundColor = .blue
        self.footer.backgroundColor = .red
    }
    
    func getRandomColor() -> UIColor{
        
        let randomRed:CGFloat = CGFloat(drand48())
        
        let randomGreen:CGFloat = CGFloat(drand48())
        
        let randomBlue:CGFloat = CGFloat(drand48())
        
        return UIColor(red: randomRed, green: randomGreen, blue: randomBlue, alpha: 1.0)
        
    }
    
    var lastV: UIView?
    var bottom: Constraint?
    var isFirst = true
    
    func action() {
        
        //simulate the askforcard
        if isFirst {
            self.containerView.addSubview(self.footer)
            
            self.footer.snp.makeConstraints { (make) in
                make.leading.trailing.equalToSuperview()
                make.bottom.equalToSuperview()
                make.width.equalToSuperview()
                make.height.greaterThanOrEqualTo(150.0)
            }
            
            isFirst = false
            
            return
        }
        
        //ading views
        let view = UIView()
        
        self.containerView.addSubview(view)
        
        self.bottom?.deactivate()
    
        view.snp.makeConstraints { (make) in
            make.left.right.equalTo(self.containerView)
            make.width.equalTo(self.containerView)
            make.height.equalTo(200.0)
            
            if let v = self.lastV {
                make.top.equalTo(v.snp.bottom)
            } else {
                make.top.equalTo(self.header.snp.bottom)
            }
            
            self.bottom = make.bottom.equalTo(self.footer.snp.top).constraint
        }
        
        view.backgroundColor = self.getRandomColor()
        self.lastV = view
    }
    
}

